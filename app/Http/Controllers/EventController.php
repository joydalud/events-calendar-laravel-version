<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{

    public function store(Request $request)
    {   
        

        try {

            foreach ($request->events as $value) {  

                $event = new Event;

                $event->month = $value["month"];
                $event->date = $value["date"];
                $event->year = $value["year"];
                $event->name = $value["event"];
                
                $event->save();
            }

            $result = array("success" => 1);
            return $result;
        }
        catch (Exception $e) {
            $result = array("success" => 0);
            return $result;
        }
    }


}
