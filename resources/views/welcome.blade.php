<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

        <!--Toastr-->
        <link rel="stylesheet" type="text/css" href="{{asset('css/toastr.min.css')}}">

    </head>
    <body>
        <div id="app">
            <h1>Calendar</h1>
            <div class="content">
                <div class="sidebar">

                <form>
                    <div class="form-row">
                        <div class="form-group" style="width: 100%;">
                            <label for="event">Event</label>
                            <input type="text" class="form-control" id="event" >
                        </div>
                    </div>    

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="start_date">From</label>
                            <input type="text" class="form-control" id="start_date" name="start_date" autocomplete="off" required>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="end_date">To</label>
                            <input type="text" class="form-control" id="end_date" name="end_date" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input days" type="checkbox" id="mon" name="days" value="mon">
                            <label class="form-check-label" for="mon">Mon</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input days" type="checkbox" id="tue" name="days" value="tue">
                            <label class="form-check-label" for="tue">Tue</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input days" type="checkbox" id="wed" name="days" value="wed">
                            <label class="form-check-label" for="wed">Wed</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input days" type="checkbox" id="thu" name="days" value="thu">
                            <label class="form-check-label" for="thu">Thu</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input days" type="checkbox" id="fri" name="days" value="fri">
                            <label class="form-check-label" for="fri">Fri</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input days" type="checkbox" id="sat" name="days" value="sat">
                            <label class="form-check-label" for="sat">Sat</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input days" type="checkbox" id="sun" name="days" value="sun">
                            <label class="form-check-label" for="sun">Sun</label>
                        </div>
                    </div>

                    <button type="button" id="btnSave" class="btn btn-primary mt-4">Save</button>

                </form>

                </div> <!--sidebar-->

                <div class="main">   
                    <h3 class="monthName"> </h3>

                    <div id="date-tbl"></div>
                </div><!--main-->
            </div><!--content-->    
        </div><!--app-->

        
        <script src="{{ asset('js/jquery-3.3.1.js') }}" ></script>
        <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/toastr.min.js')}}"></script>
        
        <script type="text/javascript">
            $("#btnSave").click(function(){
                markDate();
            });

            let nowDate = new Date();
            let today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

            let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
            let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            let startDate = '';

            $('#start_date').datepicker({
                format: 'mm-dd-yyyy',
                onClose: function (selected) {
                    startDate = selected;
                    $("#end_date").datepicker("option", "minDate", selected);
                },
                onSelect: function() {
                    $('#end_date').datepicker('enable');
                }                
            });

            $("#end_date").datepicker({
                format: 'mm-dd-yyyy',
                minDate: today,
                maxDate: '+1M',
                onSelect: function(dateText) {
                    let from = new Date(startDate);
                    let to = new Date(dateText);
                    
                    $(".monthName").text(monthNames[from.getMonth()] +" "+ to.getFullYear());
                    
                    let diffTime = Math.abs(to - from);
                    let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    let tbl = document.querySelector("#date-tbl");
                    let temp = from.getMonth();

                    document.querySelector("#date-tbl").innerHTML = '';

                    for(let d = 0; d < diffDays; d++){
                        let d = from; 
                        let child = document.createElement("DIV");
                        
                        if(temp != d.getMonth()){
                            let hdr = document.createElement("H3");
                            hdr.className = "monthName";
                            hdr.innerHTML =  this.monthNames[d.getMonth()] +" "+ d.getFullYear();
                            tbl.appendChild(hdr);
                        }

                        child.className = "daterow "+days[d.getDay()].toLowerCase();
                        child.innerHTML = "<p>"+d.getDate() + ' ' + days[d.getDay()] + "</p><p data-month='"+ (d.getMonth()+1) +"' data-day='"+ d.getDate() +"' class='ename'></p>";
                        tbl.appendChild(child);

                        temp = d.getMonth();

                        d.setDate(d.getDate() + 1);
                        
                    }                    

                }
            }).datepicker('disable');

            function markDate() {

                var checkedValue = [];
                $.each($("input[name='days']:checked"), function(){
                    checkedValue.push($(this).val());
                });

                if($("#event").val() == "" || $("#start_date").val() == "" || $("#end_date").val() == "" || checkedValue.length == 0){
                    toastr.error("Please Complete the Form!")
                    return false;
                }

                let eventList = [];
                let to = new Date($("#end_date").val());
                let daterow = document.querySelectorAll(".daterow");
                let event = $("#event").val();
                
                daterow.forEach(function(value){
                  if(value.classList.contains("active")){
                      value.classList.remove("active");
                      value.childNodes[1].innerHTML = '';
                  }
                });

                checkedValue.forEach(function (value) {
                    let dayClass = value;
                    let el = document.querySelectorAll("."+dayClass+" .ename");
                    
                    el.forEach(function(value){
                        value.innerHTML = event;
                        value.parentNode.classList.add("active");
                        eventList.push(
                            {
                            "month":value.getAttribute("data-month"), 
                            "date":value.getAttribute("data-day"),
                            "year": to.getFullYear(),
                            "event": event
                            }
                        );
                    });

                    eventList.sort((a, b) => (a.month > b.month) ? 1 : -1)

                });

                    let formData = JSON.stringify({"events":eventList});

                    //Select token with getAttribute
                    const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                    
                    fetch('/event/store',
                        {
                            headers: {
                            "Content-Type": "application/json",
                            "X-CSRF-TOKEN": token
                            },
                        method: 'post',
                        credentials: "same-origin",
                        body: formData
                        }                    
                    )
                    .then(response => response.json())
                    .then((json) => {
                        if(json.success){
                            toastr.success("Event Saved!");
                        }else{
                            toastr.error("Error Saving!");
                        }
                    });


                //console.log(eventList);

            }
        </script>

    </body>
</html>
